const randomChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

export const getShortRandomString = () => {
  const chars: number[] = [];

  for (let i = 0; i < 12; i++) {
    chars.push(randomChars.charCodeAt(Math.floor(Math.random() * randomChars.length)));
  }

  return String.fromCharCode.apply(null, chars);
};
