import {Router, stringifyQuery} from "vue-router";
import {ref, Ref} from "vue";
import {getShortRandomString} from "./secure";

export interface JGAuthClient {
  user: Ref<string>;
  loading: Ref<boolean>;
  authenticated: Ref<boolean>;
  login(): void;
  logout(): void;
}

const createClient = (
  clientId: string,
  clientSecret: string,
  baseUrl: string,
  callbackPath: string,
  router: Router,
): JGAuthClient => {
  const user: Ref<string> = ref("");
  const loading: Ref<boolean> = ref(false);
  const authenticated: Ref<boolean> = ref(false);

  router.beforeEach((to, from, next) => {
    if (to.path === callbackPath) {
      console.log(to.query["request_code"]);
      console.log(to.query["token"]);
      next("/");
    } else {
      next();
    }
  });

  const login = () => {
    const state = getShortRandomString();
    const nonce = getShortRandomString();

    const query = stringifyQuery({
      clientId,
      redirectUri: baseUrl + callbackPath,
      scope: "profile",
      state,
      nonce,
    });

    const url = `https://ouath.jguevara.dev/oauth/login${query}`;

    localStorage.setItem("oauth-state", state);

    window.location.replace(url);
  };

  const logout = () => {
    loading.value = false;
    user.value = "";
    authenticated.value = false;
    loading.value = false;
  };

  return {
    user,
    loading,
    authenticated,
    login,
    logout,
  };
};

export default createClient;
