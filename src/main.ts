import type {App, Plugin} from "vue";
import {inject} from "vue";
import {Router} from "vue-router";
import createClient, {JGAuthClient} from "./client";

interface JGAuthOptions {
  router: Router;
  clientId: string;
  clientSecret: string;
  callbackPath: string;
  baseUrl: string;
}

export const createAuthentication = (options: JGAuthOptions): Plugin => ({
  install(app: App) {
    const {router, clientId, clientSecret, baseUrl, callbackPath} = options;

    app.provide("jgauth", createClient(clientId, clientSecret, baseUrl, callbackPath, router));
  },
});

export const useAuthentication = (): JGAuthClient => {
  return inject("jgauth") as JGAuthClient;
};
